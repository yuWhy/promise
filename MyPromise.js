const isFunction = variable => typeof variable === 'function'
const PENDING = 'PENDING'
const FULFILLED = 'FULFILLED'
const REJECTED = 'REJECTED'

class MyPromise {
    constructor(handle) {
        if (!isFunction(handle)) {
            throw new Error('mypromise must accept a function as a parameter')
        }
        this._status = PENDING
        this._value = undefined

        this._fulfilledQueues = []
        this._rejectQueues = []
        try {
            handle(this._resolve.bind(this), this._reject.bind(this))
        } catch (err) {
            this._reject(err)
        }
    }

    // 改变状态和值
    _resolve(val) {


        const run = () => {
            if(this._status !== PENDING) return
            this._status = FULFILLED
            this._value = val
            const runFulfilled = (value) => {
                let cb;
                while(cb = this._fulfilledQueues.shift()) {
                    cb(value)
                }
            }
            const runRejected = (error) => {
                let cb;
                while(cb = this._rejectQueues.shift()) {
                    cb(error)
                }
            }
            if(val instanceof MyPromise) {
                val.then(value => {
                    this._value = value
                    runFulfilled(value)
                }, err => {
                    this._value = err;
                    runRejected(value)
                })
            } else {
                this._value = val;
                runFulfilled(val)
            }
        }
        setTimeout(() => run(), 0)
    }
    _reject(err) {
        if(this._status !== PENDING) return
        const run = () => {
            this._status = REJECTED
            this._value = err
            let cb; 
            while(cb = this._rejectQueues.shift()) {
                cb(err);
            }
        }
        setTimeout(run, 0)
    }
    then(onFulfilled, onRejected) {
        const { _value, _status } = this
        return new MyPromise((onFulfilledNext, onRejectedNext) => {
            let fulfilled = value => {
                try {
                    if(!isFunction(onFulfilled)) {
                        onFulfilled(value)
                    } else {
                        let res = onFulfilled(value);
                        if(res instanceof MyPromise) {
                            res.then(onFulfilledNext, onRejectedNext)
                        } else {
                            onFulfilledNext(res)
                        }
                    }
                } catch (err) {
                    onRejectedNext(err)
                }
            }
            let rejected = error => {
                try {
                    if(!isFunction(onRejected)) {
                        onRejectedNext(error)
                    } else {
                        let red = onRejected(error);
                        if(res instanceof MyPromise) {
                            res.then(onFulfilledNext, onRejectedNext)
                        } else {
                            onFulfilledNext(res)
                        }
                    }
                } catch(err) {
                    onRejectedNext(err)
                }
            }
            switch(_status) {
                case PENDING:
                    this._fulfilledQueues.push(onFulfilled);
                    this._rejectQueues.push(onRejected);
                    break
                case FULFILLED:
                    onFulfilled(_value)
                    break
                case REJECTED:
                    onRejected(_value)
                    break
            }
        })
    }
}